package main

import (
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/e4abd79/caca-heroku/internal/cmdcount"
	"gitlab.com/e4abd79/caca-heroku/internal/cmdleaderboard"
	"gitlab.com/e4abd79/caca-heroku/internal/common"
	"gitlab.com/e4abd79/caca-heroku/internal/vacation"
)

func autoUpdater() {
	for {
		common.CommandList().Update()
		time.Sleep(time.Minute)
	}
}

func main() {
	flag.Parse()
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMicro
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.StampMilli})
	rand.Seed(time.Now().UnixNano())

	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal().Msg("$PORT must be set")
	}

	go autoUpdater()

	e := echo.New()

	root_resp := "BorpaLookingAtYou\n\nUsage:\n\n"
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, root_resp)
	})

	root_resp += `/keepalive
	keepalive pings the heroku dyno so it doesn't go to sleep.
	There is no response.
`
	e.GET("/keepalive", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})

	root_resp += `/whatis[?query1[&query2[&...]]]
	whatis returns the response bodies corresponding to the provided
	commands (as query parameters) listed in the public-commands file.
	The responses are labeled but ordered arbitrarily.
`
	e.GET("/whatis", func(c echo.Context) error {
		p := c.QueryParams()
		retval := ""

		cmdlist := common.CommandList()
		cmdlist.Lock.RLock()
		defer cmdlist.Lock.RUnlock()

		for k := range p {
			resp, has := cmdlist.Responses[k]
			if !has {
				resp = "Not Found"
			}
			retval += fmt.Sprintf("[%s]: %s\n", k, resp)
		}
		return c.String(http.StatusOK, retval)
	})

	root_resp += cmdcount.Description
	e.GET("/cmdcount", cmdcount.Entry)

	root_resp += cmdleaderboard.Description
	e.GET("/cmdleaderboard", cmdleaderboard.Entry)

	root_resp += vacation.Description
	e.GET("/vacation", vacation.Entry)

	e.Logger.Fatal(e.Start(":" + port))
}
