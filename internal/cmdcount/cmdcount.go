package cmdcount

import (
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"unicode/utf8"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"gitlab.com/e4abd79/caca-heroku/internal/common"
	"gitlab.com/e4abd79/caca-heroku/internal/util"
)

type queryResult struct {
	match   string
	count   int
	subcmds []interface{}
}

type subcommands []interface{}

func (s subcommands) Len() int {
	return len(s)
}

func (s subcommands) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s subcommands) Less(i, j int) bool {
	a_is_number := false
	b_is_number := false

	switch s[i].(type) {
	case int64:
		a_is_number = true
	}

	switch s[j].(type) {
	case int64:
		b_is_number = true
	}

	if a_is_number && b_is_number {
		return s[i].(int64) < s[j].(int64)
	} else if !a_is_number && !b_is_number {
		return s[i].(string) < s[j].(string)
	} else if a_is_number {
		return true
	} else {
		return false
	}
}

// TODO: is passing cur_subcmd as string too inefficient?
func recurseSubcmds(trie *util.Trie, cur_subcmd string, subcmds []interface{}) []interface{} {
	if trie.End {
		num, err := strconv.ParseInt(cur_subcmd, 10, 32)
		if err == nil {
			subcmds = append(subcmds, num)
		} else {
			subcmds = append(subcmds, cur_subcmd)
		}
	}

	for k, next := range trie.Children {
		subcmds = recurseSubcmds(next, cur_subcmd+string(k), subcmds)
	}

	return subcmds
}

func queryTrie(trie *util.Trie, query string) queryResult {
	retval := queryResult{}
	cur := trie

	for i, k := range query {
		next, has := cur.Children[k]

		if !has || (i > 3 && cur.HasDigitSuffix) {
			break
		} else {
			retval.match += string(k)
			cur = next
		}
	}

	// If all subcommands have a matching prefix, find it.
	for !cur.End && len(cur.Children) == 1 {
		for k, next := range cur.Children {
			retval.match += string(k)
			cur = next
		}
	}
	retval.count = cur.N

	retval.subcmds = recurseSubcmds(cur, "", retval.subcmds)
	sort.Sort(subcommands(retval.subcmds))

	i := 0
CONDENSE:
	for i < len(retval.subcmds) {
		switch retval.subcmds[i].(type) {
		case int64:
			// do not break out of CONDENSE
		default:
			break CONDENSE
		}

		j := i + 1
	FINDSEQUENCE:
		for j < len(retval.subcmds) {
			switch retval.subcmds[j].(type) {
			case int64:
				// do not break out of FINDSEQUENCE
			default:
				break FINDSEQUENCE
			}

			if retval.subcmds[j].(int64)-1 != retval.subcmds[j-1].(int64) {
				break
			}

			j++
		}

		if j-i > 1 {
			tmp := retval.subcmds[:i]
			tmp = append(tmp, fmt.Sprintf("%v-%v", retval.subcmds[i], retval.subcmds[j-1]))
			tmp = append(tmp, retval.subcmds[j:]...)
			retval.subcmds = tmp
		}

		i++
	}

	return retval
}

const Description = `/cmdcount[?name=]
	cmdcount returns the total number of commands if the optional "name"
	query parameter is not provided. If "name" is defined, cmdcount
	returns the number of commands and a list of subcommands
	corresponding to the longest prefix match.

	The subcommands list is omitted if it is too long (>20 items or >100 chars).
`

func Entry(c echo.Context) error {
	log.Trace().Msg("[cmdcount::Entry] entered")
	defer log.Trace().Msg("[cmdcount::Entry] exited")

	cmdlist := common.CommandList()

	name := c.QueryParam("name")
	log.Debug().Msgf("[cmdcount::Entry] raw query='%s'", name)

	name = strings.TrimSpace(name)
	name = strings.ToLower(name)
	name = strings.ReplaceAll(name, "@", "")
	log.Debug().Msgf("[cmdcount::Entry] parsed query='%s'", name)

	cmdlist.Lock.RLock()
	defer cmdlist.Lock.RUnlock()

	var retval string
	if len(name) == 0 {
		retval = strconv.Itoa(cmdlist.Trie.N)
	} else {
		result := queryTrie(cmdlist.Trie, name)
		list := fmt.Sprintf("%v", result.subcmds)
		list = strings.ReplaceAll(list, " ", ",")

		// if utf8.RuneCountInString(result.match) < 3 ||
		if len(result.subcmds) > 20 ||
			utf8.RuneCountInString(list) > 100 {
			list = "[list omitted]"
		}

		retval = fmt.Sprintf("'%s' count: %d %v", result.match, result.count, list)
	}

	retval = fmt.Sprintf("%s (Updated %s ago)", retval, cmdlist.LastUpdateTime())
	log.Debug().Msgf("[cmdcount::Entry] returned '%s'", retval)
	return c.String(http.StatusOK, retval)
}
