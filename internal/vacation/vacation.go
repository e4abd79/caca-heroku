package vacation

import (
	"fmt"
	"math/rand"
	"net/http"
	"regexp"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"gitlab.com/e4abd79/caca-heroku/internal/common"
)

const Description = `/vacation
	vacation returns the response of a randomly chosen location from
	!locationsraw.

	$(references pinguser) should come before the API response in the
	Fossabot command.
`

func Entry(c echo.Context) error {
	cmdlist := common.CommandList()

	user := c.QueryParam("user")
	if len(user) == 0 {
		user = c.QueryParam("rnduser")
	}

	cmdlist.Lock.RLock()
	defer cmdlist.Lock.RUnlock()

	locraw, prs := cmdlist.Responses["locationsraw"]
	if !prs {
		return c.String(http.StatusFailedDependency, "Error: either !locationsraw is missing or the API needs a few seconds to wake up GoodMorning")
	}

	listsep := regexp.MustCompile(`,\s+`)
	locs := listsep.Split(strings.TrimSpace(locraw), -1)
	rand.Shuffle(len(locs), func(i, j int) { locs[i], locs[j] = locs[j], locs[i] })

	for _, loc := range locs {
		locresp, prs := cmdlist.Responses[loc]
		if prs {
			response := fmt.Sprintf("%s is on vacation [in/at] [the] %s %s", user, loc, locresp)
			return c.String(http.StatusOK, response)
		} else {
			log.Debug().Msgf("[vacation]: command '%s' does not exist", loc)
		}
	}

	return c.String(http.StatusInternalServerError, "#remind elvout [api:vacation]: location search is broken")
}
