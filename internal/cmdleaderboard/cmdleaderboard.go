package cmdleaderboard

import (
	"fmt"
	"net/http"
	"sort"
	"unicode/utf8"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"gitlab.com/e4abd79/caca-heroku/internal/common"
	"gitlab.com/e4abd79/caca-heroku/internal/util"
)

var leaderboardExclusions map[string]bool = make(map[string]bool)

type leaderboardEntry struct {
	prefix string
	count  int
}

type leaderboard []leaderboardEntry

func (lb leaderboard) Len() int {
	return len(lb)
}

func (lb leaderboard) Swap(i, j int) {
	lb[i], lb[j] = lb[j], lb[i]
}

func (lb leaderboard) Less(i, j int) bool {
	// Sort in decreasing order
	return lb[i].count > lb[j].count
}

func minInt(a, b int) int {
	// lmfao golang
	if a <= b {
		return a
	}
	return b
}

func recurseTrie(trie *util.Trie, prefix string, lb leaderboard) leaderboard {
	if utf8.RuneCountInString(prefix) > 2 && trie.HasDigitSuffix && !leaderboardExclusions[prefix] {
		return append(lb, leaderboardEntry{prefix, trie.N})
	}

	for k, next := range trie.Children {
		lb = recurseTrie(next, prefix+string(k), lb)
	}

	return lb
}

func getLeaderboard(trie *util.Trie) string {
	lb := recurseTrie(trie, "", make(leaderboard, 0))
	sort.Sort(lb)

	retval := "Fossabot command leaderboard DinkDonk $(newline) "

	last_rank := -1
	last_count := -1
	for i := 0; i < minInt(len(lb), 10); i++ {
		var rank int
		if lb[i].count == last_count {
			rank = last_rank
		} else {
			rank = i + 1
		}

		retval += fmt.Sprintf("%d. %d - %s $(newline) ", rank, lb[i].count, lb[i].prefix)

		last_count = lb[i].count
		last_rank = rank
	}

	return retval
}

const Description = `/cmdleaderboard
	cmdleaderboard returns a list of the users with the greatest number
	of registered commands. The prefix heuristic isn't perfect so some
	users may be overcounted.

	Prefix heuristic: prefix has >= 3 characters and there is at least
	one command with the structure [prefix][digit][...]
`

func Entry(c echo.Context) error {
	log.Trace().Msg("[cmdleaderboard::Entry] entered")
	defer log.Trace().Msg("[cmdleaderboard::Entry] exited")

	if len(leaderboardExclusions) == 0 {
		leaderboardExclusions["hasan"] = true
	}

	cmdlist := common.CommandList()
	cmdlist.Lock.RLock()
	defer cmdlist.Lock.RUnlock()

	return c.String(http.StatusOK, getLeaderboard(cmdlist.Trie))
}
