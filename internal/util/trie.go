package util

type Trie struct {
	Value          rune
	Children       map[rune]*Trie
	N              int
	End            bool
	HasDigitSuffix bool
}

func NewTrie(v rune) *Trie {
	return &Trie{
		v,
		make(map[rune]*Trie),
		0,
		false,
		false,
	}
}

func isDigit(r rune) bool {
	return 48 <= r && r <= 57
}

func (trie *Trie) Insert(s string) {
	cur := trie
	cur.N++

	for _, k := range s {
		if isDigit(k) {
			cur.HasDigitSuffix = true
		}

		next, has := cur.Children[k]
		if has {
			cur = next
		} else {
			cur.Children[k] = NewTrie(k)
			cur = cur.Children[k]
		}
		cur.N++
	}

	cur.End = true
}
