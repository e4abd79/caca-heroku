package util

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
)

type CommandList struct {
	Lock      sync.RWMutex
	Trie      *Trie
	Responses map[string]string
	//
	lastUpdateContentLen int64
	lastUpdateTimestamp  time.Time
}

func NewCommandList() *CommandList {
	return &CommandList{Trie: NewTrie(0)}
}

// LastUpdateTime returns a formatted duration since the last update.
// The caller is responsible for locking the CommandList.
func (list *CommandList) LastUpdateTime() string {
	return time.Since(list.lastUpdateTimestamp).Truncate(time.Second).String()
}

func (cmdlist *CommandList) Update() {
	const FOSSA_URL = "https://api-v1.fossabot.com/api/v1/saca__/public-commands"

	// Determine whether commands have changed using content-length.
	resp, err := http.Head(FOSSA_URL)
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}
	new_content_len, _ := strconv.ParseInt(resp.Header.Get("content-length"), 10, 32)
	cmdlist.Lock.RLock()
	old_content_len := cmdlist.lastUpdateContentLen
	cmdlist.Lock.RUnlock()
	if new_content_len == old_content_len {
		return
	}

	// Update the command list.
	resp, err = http.Get(FOSSA_URL)
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	var fossaresp FossaResponse
	json.Unmarshal(body, &fossaresp)

	newTrie := NewTrie(0)
	newResponses := make(map[string]string, 0)
	for _, cmd := range fossaresp.Commands {
		newTrie.Insert(cmd.Name)
		newResponses[cmd.Name] = cmd.Response
	}

	cmdlist.Lock.Lock()
	cmdlist.lastUpdateContentLen = new_content_len
	cmdlist.lastUpdateTimestamp = time.Now()
	cmdlist.Trie = newTrie
	cmdlist.Responses = newResponses
	cmdlist.Lock.Unlock()

	log.Info().Msgf("[cmdlist::Update] Command List Updated. New Size: %d", cmdlist.Trie.N)
}
