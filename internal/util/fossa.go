package util

type FossaChannel struct {
	Avatar      string `json:"avatar"`
	Provider    string `json:"provider"`
	ProviderId  string `json:"providerId"`
	DisplayName string `json:"displayName"`
	Login       string `json:"login"`
}

type FossaCommand struct {
	MinUserlevel int    `json:"minUserlevel"`
	Id           string `json:"_id"`
	Name         string `json:"name"`
	Response     string `json:"response"`
}

type FossaResponse struct {
	ChannelName string         `json:"channelName"`
	Channel     FossaChannel   `json:"channel"`
	Commands    []FossaCommand `json:"commands"`
}
