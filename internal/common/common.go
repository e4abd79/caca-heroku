package common

import "gitlab.com/e4abd79/caca-heroku/internal/util"

var cmdlist *util.CommandList = util.NewCommandList()

func CommandList() *util.CommandList {
	return cmdlist
}
